﻿namespace WasmSharp
{
    using Microsoft.CodeAnalysis.CSharp;
    using WasmSharp.WebAssembly.Core;

    public class Program
    {
        public static void Main(string[] args)
        {
            var programText = File.ReadAllText(args[0]);

            var tree = CSharpSyntaxTree.ParseText(programText);
            var root = tree.GetCompilationUnitRoot();

            //var compiler = new Compiler(root);
            //compiler.Compile();

            var program = new WasmProgram();

            using (FileStream stream = File.Open("main.wasm", FileMode.Create))
            {
                var writer = new WasmWriter(stream);
                writer.Write(program);
            }
        }
    }
}
