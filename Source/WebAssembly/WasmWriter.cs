namespace WasmSharp.WebAssembly.Core
{
    using System.Text;

    public class WasmWriter
    {
        private readonly BinaryWriter writer;

        /// <summary>
        /// Initializes a new instance of the <see cref="WasmWriter"/> class.
        /// </summary>
        /// <param name="stream">The binary stream to write to.</param>
        public WasmWriter(Stream stream)
        {
            
            this.writer = new BinaryWriter(stream, Encoding.UTF8, false);
        }

        public void Write(WasmProgram wasmProgram)
        {
            writer.Write(WasmProgram.Magic);
            writer.Write(WasmProgram.Version);
        }
    }
}
