namespace WasmSharp.WebAssembly.Core
{
    public struct WasmModule { }

    public struct WasmProgram
    {
        /// <summary>
        /// The magic number that represents a WebAssembly binary file.
        /// </summary>
        public const int Magic = 0x6d736100;

        /// <summary>
        /// The WebAssembly version.
        /// </summary>
        public const int Version = 0x00000001;

        public List<WasmModule> modules = new List<WasmModule>();

        public WasmProgram() { }
    }
}
