namespace WasmSharp.AST
{
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;

    public class Compiler
    {
        private readonly CSharpSyntaxNode root;

        public Compiler(CSharpSyntaxNode root)
        {
            this.root = root;
        }

        public void Compile()
        {
            switch (this.root)
            {
                default:
                    break;
            }
        }
    }
}
